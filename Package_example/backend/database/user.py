''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Necessary Imports
import mysql.connector as mysql                   # Used for interacting with the MySQL database
import os                                         # Used for interacting with the system environment
from dotenv import load_dotenv                    # Used to read the credentials
import bcrypt
from backend.auth import AuthHandler

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Configuration
temp = load_dotenv('credentials.env')                 # Read in the environment variables for MySQL
db_config = {
  "host": os.environ['MYSQL_HOST'],
  "user": os.environ['MYSQL_USER'],
  "password": os.environ['MYSQL_PASSWORD'],
  "database": os.environ['MYSQL_DATABASE']
}
session_config = {
  'session_key': os.environ['SESSION_KEY']
}
AuthHandler = AuthHandler()

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Define helper functions for CRUD operations
# CREATE SQL query
def create_user(first_name:str, last_name:str, student_id:int, email:str, username:str,  password:str, pin:int) -> int:
  password = AuthHandler.get_password_hash(password)
  pin = AuthHandler.get_password_hash(str(pin))
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  query = """insert into users (first_name, last_name, student_id, email, username, password, pin)
            values (%s, %s, %s, %s, %s, %s, %s)"""
  values = (first_name, last_name, student_id, email, username, password, pin)
  cursor.execute(query, values)
  db.commit()
  db.close()
  return cursor.lastrowid

# SELECT SQL query
def select_users(username:str=None) -> list:
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  if username == None:
    query = f"select id, first_name, last_name, username from users;"
    cursor.execute(query)
    result = cursor.fetchall()
  else:
    query = f"select id, first_name, last_name, username, email, student_id from users where username='{username}';"
    cursor.execute(query)
    result = cursor.fetchone()
  db.close()
  return result

'''
update password query
'''
def update_pass(username:str, password:str):
  password = AuthHandler.get_password_hash(password)
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  query = "update users set password=%s where username=%s;"
  values = (password, username)
  cursor.execute(query, values)
  db.commit()
  db.close()
  return True if cursor.rowcount == 1 else False

# DELETE SQL query
def delete_user(user_id:int) -> bool:
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  cursor.execute(f"delete from users where id={user_id};")
  db.commit()
  db.close()
  return True if cursor.rowcount == 1 else False

# SELECT query to verify hashed password of users
def check_user_password(username:str, password:str) -> bool:
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  query = 'select password from users where username=%s'
  cursor.execute(query, (username,))
  result = cursor.fetchone()
  cursor.close()
  db.close()

  if result is not None:
    return AuthHandler.verify_password(password, result[0])
  return False


#For forget password
def check_user_pin(username:str, pin:str) -> bool:
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  query = 'select pin from users where username=%s'
  cursor.execute(query, (username,))
  result = cursor.fetchone()
  cursor.close()
  db.close()

  if result is not None:
    return AuthHandler.verify_password(pin, result[0])
  return False

def reset_pass(username: str, reset_password:str):
  try:
    reset_password = AuthHandler.get_password_hash(reset_password)
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = "update users set password=%s where username=%s;"
    values = (reset_password, username)
    cursor.execute(query, values)
    db.commit()
    return {'success':True, 'message': 'Password Change successful'}
  except:
    return {'success':False, 'message': 'Password Change unsuccessful'}

'''
method to check if user exists
'''
def check_user_exist(username: str):
  db = mysql.connect(**db_config)
  cursor = db.cursor()
  query = 'select * from users where username=%s'
  cursor.execute(query, (username,))
  result = cursor.fetchone()
  cursor.close()
  db.close()

  if result is not None:
    return True
  return False

'''
method to check if email exists
'''
def check_user_email(email: str):
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = 'select email from users;'
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    db.close()
    if (email,) in result:
      return True
    return False
  