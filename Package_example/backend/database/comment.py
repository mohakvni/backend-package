''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Necessary Imports
from fastapi import Depends
import mysql.connector as mysql                   # Used for interacting with the MySQL database
import os                                         # Used for interacting with the system environment
from dotenv import load_dotenv                    # Used to read the credentials
from backend.auth import AuthHandler

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Configuration
temp = load_dotenv('credentials.env')                 # Read in the environment variables for MySQL
db_config = {
  "host": os.environ['MYSQL_HOST'],
  "user": os.environ['MYSQL_USER'],
  "password": os.environ['MYSQL_PASSWORD'],
  "database": os.environ['MYSQL_DATABASE']
}
session_config = {
  'session_key': os.environ['SESSION_KEY']
}
AuthHandler = AuthHandler()

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''
SQL method to create comment
'''
def create_comment(idea:str, comment:str, username:str):
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = "insert into comments (username, idea, comment) values ('{username}','{idea}','{comment}');".format(
        username = username,
        idea = idea,
        comment = comment
    )
    print(query)
    cursor.execute(query)
    db.commit()
    db.close()
    
    return cursor.lastrowid

'''
SQL method to get all comments
'''
def get_comments():
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = "select * from comments;"
    cursor.execute(query)
    data = cursor.fetchall()
    db.close()
    return data

'''
SQL method to get a comment
'''
def get_comment(id):
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = "select * from comments where comment_id={id};".format(id=id)
    cursor.execute(query)
    data = cursor.fetchone()
    db.close()
    return data

'''
SQL method to delete a comment
'''
def delete_comment(comment_id):
    db = mysql.connect(**db_config)
    cursor = db.cursor()
    query = "delete from comments where comment_id={comment_id}".format(comment_id = comment_id)
    cursor.execute(query)
    db.commit()
    db.close()
    return True