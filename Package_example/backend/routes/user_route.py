'''
Required imports
'''
from backend.auth import AuthHandler
from pydantic import BaseModel
from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.responses import HTMLResponse
from ..database import user
from fastapi.templating import Jinja2Templates    # Used for generating HTML from templatized files
from fastapi.staticfiles import StaticFiles 
from backend.sessions import Sessions
sessions = Sessions(user.db_config, secret_key=user.session_config['session_key'], expiry=3600)

'''
necesssary declarations and models
'''
router = APIRouter()
views = Jinja2Templates(directory='views')        # Specify where the HTML files are located
static_files = StaticFiles(directory='public')    # Specify where the static files are located
router.mount('/public', static_files, name='public') # Mount the static files directory to /public

class User_Register(BaseModel):
    first_name: str
    last_name: str
    student_id: int
    email: str
    username: str
    password: str
    pin: int

class User_login(BaseModel):
    username: str
    password: str

class User_password_reset(BaseModel):
    reset_username: str
    reset_password: str
    pin: int

class User_password_change(BaseModel):
    current_password: str
    reset_password: str
    pin: int

AuthHandler = AuthHandler()

'''
Method to register a user
'''
@router.post('/register')
def register(User: User_Register):
    try:
        if (not user.check_user_exist(User.username) and not user.check_user_email(User.email)):
            user.create_user(User.first_name, User.last_name, 
                            User.student_id, User.email,
                            User.username, User.password, User.pin)
            return {'success': True}
        else:   
            return {'success': False, 'message':'user already exists'}
    except:
        return {'success': False, 'message':'Error please try again'}

'''
Method to login a user
'''
@router.post('/login')
def login(User: User_login, request:Request, response:Response):
    if (user.check_user_exist(User.username)):
        if (not user.check_user_password(User.username, User.password)):
            return {'success': False, 'user_exist':True, 'message':'Invalid username and/or password'}
        else:
            session = sessions.get_session(request)
            if len(session) > 0:
                sessions.end_session(request, response)
            token = AuthHandler.encode_token(User.username)
            session_data = {'token':token, 'logged_in': True}
            session_id = sessions.create_session(response, session_data)
            return {'success': True, 'session_id': session_id}
    else:   
        return {'success': False, 'user_exist':False, 'message':'User Does not exists, please register'}

'''
Method to logout a user
'''
@router.post('/logout')
def post_logout(request:Request, response:Response) -> dict:
  try:
    sessions.end_session(request, response)
    return {'success':True, 'message': 'Logout successful', 'session_id': 0}
  except:
     return {'success':False, 'message': 'Logout unsuccessful'}

'''
Method to get status of a user if they are logged in
'''
@router.get('/status')
def user_status(request: Request, username=Depends(AuthHandler.auth_wrapper)) -> bool:
   if (username['status_code'] == 401):
        return False
   else:
       return True

'''
method to reset password
'''  
@router.put("/reset/password")
def password_reset(User_password_reset: User_password_reset):
    try:
        if (user.check_user_exist(User_password_reset.reset_username)):
            if (not user.check_user_pin(User_password_reset.reset_username, str(User_password_reset.pin))):
                return {'success': False, 'message':'Invalid pin, please try again'}
            else:
                return user.reset_pass(User_password_reset.reset_username, User_password_reset.reset_password)
        else:
            return {'success': False, 'message':'User Does not Exist'}
    except:
        return {'success':False, 'message': 'Error, please try again'}

'''
Method to update password
'''
@router.put("/update/password")
def password_change(User_password_change:User_password_change, request: Request, username = Depends(AuthHandler.auth_wrapper)):
    if (username['status_code'] == 401):
        return {'success': False, 'message':'User Not Logged in'}
    if (user.check_user_password(username['information'], User_password_change.current_password)
        and user.check_user_pin(username['information'], str(User_password_change.pin))):
        if (user.update_pass(username['information'], User_password_change.reset_password)):
            return {'success': True, 'message':'Passwords updated'}
        else:
            return {'success': False, 'message':'Error updating password'}
    else:
        return {'success': False, 'message':'Password/Pin do not match'}

'''
Methods to get user and register page
'''
@router.get("/register", response_class=HTMLResponse)
def get_html( request: Request) -> HTMLResponse:
  return views.TemplateResponse('Registration.html', {'request':request})

@router.get("/login", response_class=HTMLResponse)
def get_html( request: Request) -> HTMLResponse:
  return views.TemplateResponse('login.html', {'request':request})