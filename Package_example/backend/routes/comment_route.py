from backend.auth import AuthHandler
from pydantic import BaseModel
from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.responses import HTMLResponse
from ..database import comment
from fastapi.templating import Jinja2Templates    # Used for generating HTML from templatized files
from fastapi.staticfiles import StaticFiles 


comment_router = APIRouter()
AuthHandler = AuthHandler()

views = Jinja2Templates(directory='views')        # Specify where the HTML files are located
static_files = StaticFiles(directory='public')    # Specify where the static files are located
comment_router.mount('/public', static_files, name='public') # Mount the static files directory to /public

class comment_model(BaseModel):
    comment: str
    idea: str

'''
Method to get all comments
'''
@comment_router.get('/all')
def get_all_comments():
    try:
        return {'success': True, 'data': comment.get_comments()}
    except:
        return {'success': False, 'message': 'error retriving comments'}

'''
Method to create a comment
'''
@comment_router.post('/create')
def create_comment(comment_model: comment_model, request: Request, username=Depends(AuthHandler.auth_wrapper)):
    try:
        if (username['status_code'] == 401):
            return {'success': False, 'message': 'error creating comment, Not logged in'}
        data = comment.create_comment(comment_model.idea,comment_model.comment, username['information'])
        return {'success': True, 'data': data}            
    except:
        return {'success': False, 'message': 'error creating comment'}
    
'''
Method to delete a comment
'''
@comment_router.delete('/delete/{comment_id}')
def delete_comment(comment_id, username=Depends(AuthHandler.auth_wrapper)):
    if (username['status_code'] == 401):
            return {'success': False, 'message': 'error creating comment, Not logged in'}
    if (username['information'] != comment.get_comment(comment_id)[1]):
        return {'success': False, 'message':"comment cannot be deleted, user did not match"}
    if (comment.delete_comment(comment_id)):
        return {'success': True, 'message':"comment delete successfully"}
    return {'success': False, 'message':"comment could not be deleted"}
